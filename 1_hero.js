// 1.-Write a function midrange, that calculates the midrange of 3 numbers.
// The midrange is the mean of the smallest and largest number.
// url = https://www.jshero.net/en/koans/minmax.html
let min = Math.min(5, 7);
let max = Math.max(...[3, 9, 2]);
// console.log(min, max);

const midrange = (number_1, number_2, number_3) => {
    const min = Math.min(number_1, number_2, number_3);
    const max = Math.max(number_1, number_2, number_3);
    const prom = (min + max) / 2
    console.log(prom);
};
// midrange(3, 9, 1)

const midramnge_generalizado = (...arr_of_params_nums) => { // rest operator
    arr_of_nums = [];
    for (let num of arr_of_params_nums) {
        arr_of_nums.push(num)
    }
    const min = Math.min(...arr_of_nums)
    const max = Math.max(...arr_of_nums)
    const prom = (min + max) / 2
    console.log(min, max, prom);
};

arr_params = [3, 9, 1, 10, 30]
midramnge_generalizado(...arr_params); // spread operator

// 2.- aquí ira el segundo ejercicio, empieza a ver otro ejercicio